//The client class with main()
public class Foothill {

	public static void main(String[] args) {
		
		//Create 5 objects of the property class and initiate them
		Property[] property_list = {new Property("CND",235000,2,30,"55 Jackson Rd, Los Angeles"),
										 	 new Property("SFD",400000,3,22,"4321 Line St, Moorpark"),
										 	 new Property("CND",300000,1,55,"234 Foothill Rd, Los Altos"),
										 	 new Property("RNT",1200,2,43,"34 Union St, San Jose"),
										 	 new Property("RNT",800,2,20,"5235 Park Ave, San Diego")};
		
		//Print array before and after performing sorts (based on price, rooms and days)
		Property.PrintArray("Property List Before Sorting",property_list);
		Property.ArraySort(property_list,"byPrice");
		Property.PrintArray("Property List After Sorting by Price",property_list);
		Property.ArraySort(property_list,"byNumRooms");
		Property.PrintArray("Property List After Sorting by Number of Rooms",property_list);
		Property.ArraySort(property_list,"byDaysOnMarket");
		Property.PrintArray("Property List After Sorting by Days on Market",property_list);
		
	}

}
